let xp = 0;
let bug = 0;
let valides = 0;
let luck = 250;
let expRatio = 1;

document.getElementById('effId').innerHTML = expRatio;

document.onkeyup = function () {
    let randomNumber = Math.random() * 1000;
    if (randomNumber < luck) {
        M.toast({
            html: 'Ligne de code valide. Exp +1',
            classes: "green",
            displayLength: 400
        });
        xp = xp + expRatio;
        upgrade()
        valides = valides + 1
        document.getElementById('expId').innerHTML = (Math.round(xp * 100) / 100);
        document.getElementById('validesId').innerHTML = (valides)
    } else {
        M.toast({
            html: 'Ligne de code non valide. Bug +1',
            classes: "red",
            displayLength: 400
        })
        bug = bug + 1;
        document.getElementById('bugsId').innerHTML = (bug)
    }
}

function upgrade() {

    if (xp >= 10) {
        for (let i = 0; i < 1; i++) {
            let upgradeBtn1 = document.getElementById('upgradeBtn1');
            upgradeBtn1.classList.remove('disabled');
            upgradeBtn1.innerHTML = "Installer Visual Studio";
        }


    } else if (xp >= 20) {
        for (let i = 0; i < 1; i++) {
            let upgradeBtn2 = document.getElementById('upgradeBtn2');
            upgradeBtn2.classList.remove('disabled');
            upgradeBtn2.innerHTML = "Apprendre : Wordpress"
        }
    } else if (xp >= 30) {
        for(let i = 0; i < 1; i++) {
            let upgradeBtn3 = document.getElementById('upgradeBtn3');
            upgradeBtn3.classList.remove('disabled');
            upgradeBtn3.innerHTML = "Nouveau Binôme : Romain"

        }
    }
}

document.getElementById('upgradeBtn1').addEventListener("click", function () {
    luck = 275;
    expRatio = expRatio + (10 / 100);
    document.getElementById('effId').innerHTML = expRatio;
    upgradeBtn1.parentNode.removeChild(upgradeBtn1);
    let upgradeBtn2 = document.getElementById('upgradeBtn2');
    upgradeBtn2.innerHTML = "Apprendre Wordpress : 10xp";
    alert('VS Code installé. Bug -10%. Efficacité +10%');


});

document.getElementById('upgradeBtn2').addEventListener("click", function () {
    upgradeBtn2.parentNode.removeChild(upgradeBtn2);
    alert('Vous avez appris la compétence : Wordpress. Exp globale augmentée de 10%. ');
    expRatio = expRatio + (10 / 100);
    document.getElementById('effId').innerHTML = expRatio;

    upgradeBtn3.innerHTML = "Louer Binome : Romain ";
});

document.getElementById('upgradeBtn3').addEventListener("click", function () {
    upgradeBtn3.parentNode.removeChild(upgradeBtn3);
    alert("Romain rejoint l'équipe. Efficacité réduite de 50%");
    expRatio = expRatio / 2;
    document.getElementById('effId').innerHTML = expRatio;
    upgradeBtn4.innerHTML = "Louer Binome : Aurore";
});